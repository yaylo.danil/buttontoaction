package com.yaylo.buttontoaction.view

import android.content.Context
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.yaylo.buttontoaction.data.model.ButtonActions
import com.yaylo.buttontoaction.data.model.ButtonActionsItem
import com.yaylo.buttontoaction.data.network.ApiFactory
import com.yaylo.buttontoaction.data.network.isOnline
import com.yaylo.buttontoaction.databinding.ActivityMainBinding
import com.yaylo.buttontoaction.notifications.NotificationManager
import com.yaylo.buttontoaction.viewmodel.MainActivityVM
import java.util.*

class MainActivity : AppCompatActivity() {

    companion object {
        val callActionTag = "call"
        val LOG_TAG = "ACTIONS"
    }

    private lateinit var binding: ActivityMainBinding
    private val api = ApiFactory().api
    //TODO add DI support
    private val viewModel by lazy { MainActivityVM(api) }
    private val notificationManager by lazy { NotificationManager(applicationContext) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        subscribeComponents()
    }

    override fun onResume() {
        super.onResume()
        if(intent.extras?.getBoolean(callActionTag) == true) {
            //TODO add opening “choose contact” screen from https://developer.android.com/training/contacts-provider/display-contact-badge
        }
    }

    private fun subscribeComponents() {
        viewModel.buttonActions.observe(this) { actions ->
            actions?.let {
                chooseAction(it)?.let { action -> setActionToClickListener(action) }
            }
        }
    }

    private fun setActionToClickListener(action: ButtonActionsItem) {
        Log.d(LOG_TAG, action.type)
        when (action.type) {
            Actions.TOAST.name.lowercase() -> {
                binding.buttonToAction.setOnClickListener {
                    Toast.makeText(
                        applicationContext,
                        "Action is Toast!",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
            Actions.ANIMATION.name.lowercase() -> {
                binding.buttonToAction.setOnClickListener { button ->
                    button.animate().setDuration(1000).rotationBy(360F).start()
                }
            }
            Actions.CALL.name.lowercase() -> {
                binding.buttonToAction.setOnClickListener {
                    //TODO add opening “choose contact” screen from https://developer.android.com/training/contacts-provider/display-contact-badge
                }
            }
            Actions.NOTIFICATION.name.lowercase() -> {
                binding.buttonToAction.setOnClickListener {
                    notificationManager.showNotification()
                }
            }
            else -> {}
        }
    }

    private fun chooseAction(list: ButtonActions): ButtonActionsItem? {
        val sharedPref = getPreferences(Context.MODE_PRIVATE)
        val filteredList = arrayListOf<ButtonActionsItem>()
        list.forEach {
            val timeShowedBefore = sharedPref.getLong(it.type, 0)
            if (it.enabled.not()
                || timeShowedBefore + it.cool_down > System.currentTimeMillis()
                || Calendar.getInstance().get(Calendar.DAY_OF_WEEK) in it.valid_days
            ) {
            } else {
                filteredList.add(it)
            }
            if (it.type == Actions.TOAST.name.lowercase() && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!isOnline(baseContext)) {
                    filteredList.remove(it)
                }
            }
        }
        return if(filteredList.isNotEmpty()) {
            val action = filteredList.maxBy { it.priority }
            sharedPref.edit().putLong(action.type, System.currentTimeMillis()).apply()
           action
        }
        else{
            Log.d(LOG_TAG, "No enabled actions")
            null
        }
    }

    enum class Actions {
        ANIMATION,
        TOAST,
        CALL,
        NOTIFICATION
    }
}
