package com.yaylo.buttontoaction.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.yaylo.buttontoaction.data.model.ButtonActions
import com.yaylo.buttontoaction.data.network.ButtonApi
import kotlinx.coroutines.launch

class MainActivityVM(val api: ButtonApi) : ViewModel() {
    val buttonActions: MutableLiveData<ButtonActions?> = MutableLiveData(null)

    init {
        retrieveActions()
    }

    private fun retrieveActions() {
        viewModelScope.launch {
            val actions = api.getButtonActions()
            if (actions.isSuccessful) {
                buttonActions.postValue(actions.body())
            }
        }
    }
}
