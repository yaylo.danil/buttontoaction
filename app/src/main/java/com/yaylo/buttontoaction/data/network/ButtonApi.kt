package com.yaylo.buttontoaction.data.network

import com.yaylo.buttontoaction.data.model.ButtonActions
import retrofit2.Response
import retrofit2.http.GET

interface ButtonApi {
    @GET("butto_to_action_config.json")
    suspend fun getButtonActions(): Response<ButtonActions>
}
