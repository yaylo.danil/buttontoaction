package com.yaylo.buttontoaction.data.model

class ButtonActions : ArrayList<ButtonActionsItem>()

data class ButtonActionsItem(
    val cool_down: Int,
    val enabled: Boolean,
    val priority: Int,
    val type: String,
    val valid_days: List<Int>
)
